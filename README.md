## Synacor Challenge

Made by the same person as [Advent of Code]. This repository includes
the original tarball you can download from that website as well as tools
I've developed while trying to crack [the challenge].

[Advent of Code]: https://adventofcode.com/
[the challenge]: https://challenge.synacor.com/
